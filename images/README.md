Images
=====================

The images can be freely downloaded from [http://digitalcomicmuseum.com/](http://digitalcomicmuseum.com/) or SFTP (JPG + SVG). For the latter option, please request creditential by email to the authors.
