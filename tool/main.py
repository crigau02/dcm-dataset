#!/usr/bin/python
from __future__ import division
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk
import os
import glob
import random

#-------------------------------------------------------------------------------
__title__ =         "Comics character annotation tool"
__description__ =   "Annotation tool for drawing rectangle and polygon arround comic book image content (panel, face, body, balloon, text, etc.)."
__copyright__ =     "University of La Rochelle, L3i/SAIL labs"
__credits__ =       ["Christophe Rigaud","Van Nguyen","Base code is borrowed from Qiushi"]
__created__ =       "06/06/2017"
__license__ =       "CeCILL"
__version__ =       "1.3"
__maintainer__ =    "Christophe Rigaud"
__email__ =         "christophe.rigaud@univ-lr.fr"

#
#-------------------------------------------------------------------------------

# colors for the bboxes
# COLORS = ['red', 'blue', 'yellow', 'pink', 'cyan', 'green', 'black',
#         'maroon', 'orange', 'aquamarine', 'violet', 'violet red', 'sandy brown', 'sea green']
COLORS = ['green', 'red', 'black', 'purple', 'cyan', 'violet',
        'maroon', 'orange', 'blue']

HIGHLIGHT = 'blue'

#ebd_Classes = ["-", "human-like", "-", "-", "object-like", "animal-like", "figurant", "face", "panel"]
ebd_Classes = ["balloon", "human-like", "-", "-", "manga-like", "-", "animal-like", "face", "panel"]
TEST = {"-": 0, "-": 1, "human-like": 2, "-": 3, "manga-like": 4, "-": 5, "animal-like": 6, "face": 7, "panel": 8}
ext = '.jpg'

class ToolTip(object):

    def __init__(self, widget):
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0

    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 27
        y = y + cy + self.widget.winfo_rooty() +27
        self.tipwindow = tw = Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        try:
            # For Mac OS
            tw.tk.call("::tk::unsupported::MacWindowStyle",
                       "style", tw._w,
                       "help", "noActivates")
        except TclError:
            pass
        label = Label(tw, text=self.text, justify=LEFT,
                      background="#ffffe0", relief=SOLID, borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()

def createToolTip(widget, text):
    toolTip = ToolTip(widget)
    def enter(event):
        toolTip.showtip(text)
    def leave(event):
        toolTip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)

class LabelTool():
    def __init__(self, master):
        # set up the main frame
        self.parent = master
        self.parent.title("Comic characters annotation tool")
        self.frame = Frame(self.parent)
        self.frame.pack(fill=BOTH, expand=1)
        self.parent.resizable(width = FALSE, height = FALSE)

        # initialize global state
        self.imageList= []
        self.egDir = ''
        self.egList = []
        self.outDir = ''
        self.InputDir = ''
        self.cur = 0
        self.total = 0
        self.category = 0
        self.imagename = ''
        self.labelfilename = ''
        self.tkimg = None

        # initialize mouse state
        self.STATE = {}
        self.STATE['click'] = 0
        self.POLYGON_MODE = 0
        self.CLOSE_POLYGON = 0
        self.STATE['x'], self.STATE['y'] = 0, 0
        self.x_polygon, self.y_polygon = [], []

        # reference to bbox
        self.bboxIdList = []
        self.bboxId = None
        self.bboxList = []
        self.hl = None
        self.vl = None
        self.selectedBox = None

        # ----------------- GUI stuff ---------------------
        # dir entry & load
        self.label = Label(self.frame, text = "Image Dir:")
        self.label.grid(row = 0, column = 0, sticky = E)
        self.entryImgDir = Entry(self.frame)
        self.entryImgDir.insert(END, 'Images')
        self.entryImgDir.grid(row = 0, column = 1, sticky = W+E)
        # 
        self.ldBtn = Button(self.frame, text = "Load", command = self.loadDir)
        self.ldBtn.grid(row = 0, column = 2, pady=10, sticky = W+E)

        # main panel for labeling        

        self.mainPanel = Canvas(self.frame, cursor='tcross')
        self.mainPanel.bind("<Button-1>", self.mouseClick)
        self.mainPanel.bind("<Motion>", self.mouseMove)
        self.parent.bind("<Escape>", self.cancelBBox)  # press <Espace> to cancel current bbox
        #self.parent.bind("s", self.cancelBBox)
        self.parent.bind("<Left>", self.prevImage) # press 'left arrow' to go backforward
        self.parent.bind("<Right>", self.nextImage) # press 'right arrow' to go forward
        self.parent.bind("w", self.polygonMode) # switch between rectangle/polygon drawing
        self.parent.bind("f", self.closePolygon)
        self.parent.bind("c", self.updateBBox)
        self.mainPanel.grid(row = 1, column = 1, rowspan = 7, sticky = W+N)

        # list of classes bouding boxes
        self.lb2 = Label(self.frame, text = 'Classes:')
        self.lb2.grid(row = 1, column = 2, padx=10, sticky = "nsew")
        self.listboxID = Listbox(self.frame, width = 22, height = 10)
        self.listboxID.grid(row = 2, column = 2, padx=10, sticky = "nsew")  
        self.listboxID.configure(exportselection=False)  
        self.listboxID.bind('<<ListboxSelect>>', self.onSelectClass)
        index = 0  
        for item in ebd_Classes:
            self.listboxID.insert(END, item)
            self.listboxID.itemconfig(END, fg=COLORS[index])
            index += 1
        self.listboxID.select_set(8) #onload class selection
        self.current_class = 8

        # showing bouding boxes
        self.lb1 = Label(self.frame, text = 'Bounding boxes:')
        self.lb1.grid(row = 3, column = 2, padx=10, sticky = "nsew")
        self.listbox = Listbox(self.frame, width = 22, height = 25)
        self.listbox.grid(row = 4, column = 2, padx=10, pady=0, sticky = "nsew")
        # self.listbox.configure(selectmode='multiple', exportselection=False)
        self.listbox.configure( exportselection=False)
        self.listbox.bind('<<ListboxSelect>>', self.onSelectBox)
       

        # Delete & Clear buttons
        self.btnDel = Button(self.frame, text = 'Delete the selected box', command = self.delBBox)
        self.btnDel.grid(row = 5, column = 2, padx = 10, pady=0, sticky = "nsew")        
        self.btnClear = Button(self.frame, text = 'Clear all boxes', command = self.deleteAllBBox)
        self.btnClear.grid(row = 6, column = 2, padx = 10, pady=0, sticky = "nsew")    
        self.btnUpdate = Button(self.frame, text = "Change class", command = self.updateBBox)
        self.btnUpdate.grid(row = 7, column = 2, padx = 10, pady=0, sticky = "nsew")    
        createToolTip(self.btnUpdate, "To change the class of a box, simply select new class and click this btn.")  

        # control panel for image navigation 
        self.ctrPanel = Frame(self.frame)
        self.ctrPanel.grid(row = 8, column = 1, columnspan = 2, sticky = W+E)

        self.prevBtn = Button(self.ctrPanel, text='<< Prev', width = 10, command = self.prevImage)
        self.prevBtn.pack(side = LEFT, padx = 5, pady = 3)
        self.nextBtn = Button(self.ctrPanel, text='Next >>', width = 10, command = self.nextImage)
        self.nextBtn.pack(side = LEFT, padx = 5, pady = 3)
        self.progLabel = Label(self.ctrPanel, text = "Progress:     /    ")
        self.progLabel.pack(side = LEFT, padx = 5)
        self.tmpLabel = Label(self.ctrPanel, text = "Go to Image No.")
        self.tmpLabel.pack(side = LEFT, padx = 5)
        self.idxEntry = Entry(self.ctrPanel, width = 5)
        self.idxEntry.pack(side = LEFT)
        self.goBtn = Button(self.ctrPanel, text = 'Go', command = self.gotoImage)
        self.goBtn.pack(side = LEFT)

        # self.btnUpdate = Button(self.ctrPanel, text = 'Update', command = self.updateBBox)
        # self.btnUpdate.pack(side = LEFT)

        # display mouse position
        self.disp = Label(self.ctrPanel, text='')
        self.disp.pack(side = RIGHT)

        self.frame.columnconfigure(2, weight = 1)
        self.frame.rowconfigure(8, weight = 1)

    def loadDir(self, dbg = False):

        ImgDir = self.entryImgDir.get()
        self.InputDir = ImgDir
        if(ImgDir == ''):
            self.imageList = glob.glob('./Images' + "/*/*" + ext)
        else:
            self.imageList = glob.glob(ImgDir + "/*/*" + ext)

        if len(self.imageList) == 0:
            self.imageList = glob.glob(ImgDir + "/*" + ext)
            
            if len(self.imageList) == 0:
                print('No images found in the specified dir!')
                return

        # default to the 1st image in the collection
        self.cur = 1
        self.total = len(self.imageList)
         # set up output dir
        self.outDir = os.path.join(r'./Labels')
        if not os.path.exists(self.outDir):
            os.mkdir(self.outDir)
        for index in range(len(self.imageList)):
            if "0308" in self.imageList[index]:
                print(index, self.imageList[index])
            self.loadImage()
            print('%d images loaded from %s' %(self.total, ImgDir))

    def loadImage(self):
        # load image
        imagepath = self.imageList[self.cur - 1]
        self.img = Image.open(imagepath)
        width, height = self.img.size
        self.ratio = 900/float(height)
        size = (int(self.ratio*width), 900)
        resized = self.img.resize(size,Image.ANTIALIAS)

        self.tkimg = ImageTk.PhotoImage(resized)

        self.mainPanel.config(width = max(self.tkimg.width(), 400), height = 900)
        self.mainPanel.create_image(0, 0, image = self.tkimg, anchor=NW)
        self.progLabel.config(text = "%04d/%04d" %(self.cur, self.total))

        # load labels
        self.clearBBox()
        self.imagename = os.path.split(imagepath)[-1].split('.')[0]
        imagename01 = os.path.split(imagepath)[-2].split('/')[-1]
        labelname = self.imagename + '.txt'
        self.labelfilename = os.path.join(self.outDir, imagename01, labelname)
        self.labelfilename = imagepath.replace(self.InputDir, self.outDir)
        self.labelfilename = self.labelfilename.replace(".jpg", ".txt")
        print(self.labelfilename)
        dir_labelfilename = os.path.join(self.outDir, imagename01)
        if not os.path.exists(dir_labelfilename):
            os.mkdir(dir_labelfilename)
        # bbox_cnt = 0
        print(self.labelfilename)
        if os.path.exists(self.labelfilename):
            with open(self.labelfilename) as f:
                for (i, line) in enumerate(f):

                    lines = line.split()
                    if len(lines) < 5:
                        print(self.labelfilename)
                        print(line)
                        continue

                    if len(lines) > 5:
                        #lines = map(int, lines)
                        lines = [list(map(int, sublist)) for sublist in lines]
                        tmp = [x *self.ratio for x in lines]
                        self.bboxList.append(tuple(lines))
                        tmpId = self.mainPanel.create_polygon(tmp[1:],  \
                                                                width = 3, \
                                                                fill='', \
                                                                outline = COLORS[lines[0] % len(COLORS)])
                        self.bboxIdList.append(tmpId)
                        self.listbox.insert(END, '%s:  %s' %(ebd_Classes[lines[0]], line))
                        self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = 'black')
                    else:    
                        tmp = [int(lines[0]), int(lines[1]), int(lines[2]), int(lines[3]), int(lines[4])]
                        #tmp = map(int, tmp)

                        self.bboxList.append(tuple(tmp))
                        tmpId = self.mainPanel.create_rectangle(tmp[1]*self.ratio, tmp[2]*self.ratio, \
                                                                tmp[3]*self.ratio, tmp[4]*self.ratio, \
                                                                width = 3, \
                                                                outline = COLORS[tmp[0] % len(COLORS)])
                        self.bboxIdList.append(tmpId)
                        self.listbox.insert(END, '%s:  (%d, %d) -> (%d, %d)' %(ebd_Classes[tmp[0]], tmp[1], tmp[2], tmp[3], tmp[4]))
                        self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = 'black')

    def saveImage(self):
        if(self.labelfilename == ''):
            return

        with open(self.labelfilename, 'w') as f:
            for bbox in self.bboxList:
                #With classid at the end = bbox[4]
                
                if(len(bbox)>5):
                    print('{} {} {} {} {}\n'.format(bbox[0], bbox[1], bbox[2], bbox[3], bbox[4]))
                    newItem = ""
                    for idx in range (len(bbox)):
                        newItem = newItem + str(bbox[idx]) + " "
                    print(newItem)
                    f.write('{}\n'.format(newItem))
                else:
                    f.write('{} {} {} {} {}\n'.format(bbox[0], bbox[1], bbox[2], bbox[3], bbox[4]))

        print('Image No. %d saved' %(self.cur))


    def mouseClick(self, event):
        if self.POLYGON_MODE == 0:
            if self.STATE['click'] == 0:
                self.STATE['x'], self.STATE['y'] = event.x, event.y
            else:
                x1, x2 = min(self.STATE['x'], event.x), max(self.STATE['x'], event.x)
                y1, y2 = min(self.STATE['y'], event.y), max(self.STATE['y'], event.y)
                classids= self.listboxID.curselection()
                print('classids[0]: {}'.format(classids[0]))
                x1 = int(x1/self.ratio)
                x2 = int(x2/self.ratio)
                y1 = int(y1/self.ratio)
                y2 = int(y2/self.ratio)

                self.bboxList.append((classids[0], x1, y1, x2, y2))
                self.bboxIdList.append(self.bboxId)
                self.bboxId = None
                self.listbox.insert(END, '%s:  (%d, %d) -> (%d, %d)' %(ebd_Classes[classids[0]], x1, y1, x2, y2))
                self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = 'black')
            self.STATE['click'] = 1 - self.STATE['click']
        else:
            if(len(self.x_polygon) == 0):
                self.CLOSE_POLYGON = 0            
            print(self.ratio)
            print(int(event.x/self.ratio))
            self.x_polygon.append(int(event.x/self.ratio))
            self.y_polygon.append(int(event.y/self.ratio))
            print(self.x_polygon)
            print(self.y_polygon)
            total = len(self.x_polygon)
            print(total, self.CLOSE_POLYGON)
            print(event.y)
            print(self.y_polygon[total-1])
            print(self.y_polygon[total-1]*self.ratio)
            if(total > 1):
                vl = self.mainPanel.create_line(self.x_polygon[total-2]*self.ratio, self.y_polygon[total-2]*self.ratio, self.x_polygon[total-1]*self.ratio, self.y_polygon[total-1]*self.ratio, width = 2)

            if self.CLOSE_POLYGON == 1:
                print("self.CLOSE_POLYGON = 1", self.CLOSE_POLYGON)
                vl = self.mainPanel.create_line(self.x_polygon[0]*self.ratio, self.y_polygon[0]*self.ratio, self.x_polygon[total-1]*self.ratio, self.y_polygon[total-1]*self.ratio, width = 2)
                classids= self.listboxID.curselection()
                newBox = []
                newBox.append(classids[0])
                #self.bboxList.append(classids[0])
                for idx in range (len(self.x_polygon)):
                    newBox.append(self.x_polygon[idx])
                    newBox.append(self.y_polygon[idx])
                    #self.bboxList.append((self.x_polygon[idx], self.y_polygon[idx]))
                #self.bboxList.append((classids[0], x1, y1, x2, y2, x3, y3, x4, y4))
                self.bboxList.append(newBox)
                self.bboxIdList.append(self.bboxId)
                self.bboxId = None
                newItem = "id: %d" %(classids[0])
                for idx in range (len(self.x_polygon)): 
                    newItem = newItem + " (" + str(self.x_polygon[idx])+ ", " + str(self.y_polygon[idx]) + ")"
                #self.listbox.insert(END, 'id: %d (%d, %d), (%d, %d), (%d, %d), (%d, %d)' %(classids[0], x1, y1, x2, y2, x3, y3, x4, y4))
                self.listbox.insert(END, newItem)
                self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = COLORS[(len(self.bboxIdList) - 1) % len(COLORS)])
                self.x_polygon = []
                self.y_polygon = []
                self.CLOSE_POLYGON = 0

    def mouseMove(self, event):
        self.disp.config(text = 'x: %d, y: %d' %(event.x, event.y))
        if self.POLYGON_MODE == 0:
            if self.tkimg:
                if self.hl:
                    self.mainPanel.delete(self.hl)
                self.hl = self.mainPanel.create_line(0, event.y, self.tkimg.width(), event.y, width = 2)
                if self.vl:
                    self.mainPanel.delete(self.vl)
                self.vl = self.mainPanel.create_line(event.x, 0, event.x, self.tkimg.height(), width = 2)
            if 1 == self.STATE['click']:
                if self.bboxId:
                    self.mainPanel.delete(self.bboxId)
                self.bboxId = self.mainPanel.create_rectangle(self.STATE['x'], self.STATE['y'], \
                                                                event.x, event.y, \
                                                                width = 3, \
                                                                outline = COLORS[self.current_class])
        else:
            total = len(self.x_polygon)
            if(total > 0):
                if self.vl:
                    self.mainPanel.delete(self.vl)
                if self.hl:
                    self.mainPanel.delete(self.hl)
                self.vl = self.mainPanel.create_line(self.x_polygon[total-1]*self.ratio, self.y_polygon[total-1]*self.ratio, event.x, event.y, width = 2)

    def polygonMode(self, event = None):
        self.POLYGON_MODE = 1 - self.POLYGON_MODE
        self.x_polygon = []
        self.y_polygon = []
        self.CLOSE_POLYGON = 0
        print(self.POLYGON_MODE)
    
    def closePolygon(self, event = None):
        self.CLOSE_POLYGON = 1 - self.CLOSE_POLYGON
        print(self.CLOSE_POLYGON)

    def cancelBBox(self, event):
        if 1 == self.STATE['click']:
            if self.bboxId:
                self.mainPanel.delete(self.bboxId)
                self.bboxId = None
                self.STATE['click'] = 0

    def delBBox(self):
        sel = self.listbox.curselection()
        if len(sel) != 1 :
            return

        idx = int(sel[0])
        self.mainPanel.delete(self.bboxIdList[idx])
        self.bboxIdList.pop(idx)
        self.bboxList.pop(idx)
        self.listbox.delete(idx)
        if self.selectedBox:
            self.mainPanel.delete(self.selectedBox)
    
    def updateBBox(self, event = None):
        sel = self.listbox.curselection()
        #Update list BBoxes
        for idx in sel:
            classids= self.listboxID.curselection()
            newBox = list(self.bboxList[idx])
            self.bboxList[idx] = (classids[0], newBox[1], newBox[2], newBox[3], newBox[4])
            print(self.bboxIdList[idx])
            print(self.bboxList[idx])

        # Refresh listbox
        self.listbox.delete(0, len(self.bboxList))
        for idx in range(len(self.bboxIdList)):
            newBox = list(self.bboxList[idx])
            self.listbox.insert(END, '%s:  (%d, %d) -> (%d, %d)' %(ebd_Classes[newBox[0]], newBox[1], newBox[2], newBox[3], newBox[4]))
            #self.listbox.itemconfig(idx, fg = COLORS[idx % len(COLORS)])

    def clearBBox(self):        
        if self.selectedBox:
                self.mainPanel.delete(self.selectedBox)

        for idx in range(len(self.bboxIdList)):
            self.mainPanel.delete(self.bboxIdList[idx])
        self.listbox.delete(0, len(self.bboxList))
        self.bboxIdList = []
        self.bboxList = []

    def deleteAllBBox(self):
        result = messagebox.askquestion("Delete all boxes", "Are You Sure?", icon='warning')
        if result != 'yes':
            return

        if self.selectedBox:
                self.mainPanel.delete(self.selectedBox)

        for idx in range(len(self.bboxIdList)):
            self.mainPanel.delete(self.bboxIdList[idx])
        self.listbox.delete(0, len(self.bboxList))
        self.bboxIdList = []
        self.bboxList = []

    def onSelectClass(self,evt):
        # Note here that Tkinter passes an event object to onSelectBox()
        w = evt.widget
        self.current_class = int(w.curselection()[0])

    def onSelectBox(self,evt):
        # Note here that Tkinter passes an event object to onSelectBox()
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        bbox = self.bboxList[index]
        x1 = int(bbox[1])*self.ratio
        y1 = int(bbox[2])*self.ratio
        x2 = int(bbox[3])*self.ratio
        y2 = int(bbox[4])*self.ratio

        if self.tkimg:
            if self.selectedBox:
                self.mainPanel.delete(self.selectedBox)
            self.selectedBox = self.mainPanel.create_rectangle(x1, y1, \
                                                            x2, y2, \
                                                            width = 5, \
                                                            outline = "blue")
            
    def prevImage(self, event = None):
        self.saveImage()
        if self.cur > 1:
            self.cur -= 1
            self.loadImage()

    def nextImage(self, event = None):
        self.saveImage()
        if self.cur < self.total:
            self.cur += 1
            self.loadImage()

    def gotoImage(self):
        idx = int(self.idxEntry.get())
        if 1 <= idx and idx <= self.total:
            self.saveImage()
            self.cur = idx
            self.loadImage()


def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        tool.saveImage()
        root.destroy()

if __name__ == '__main__':
    root = Tk()
    root.protocol("WM_DELETE_WINDOW", on_closing)
    tool = LabelTool(root)
    root.mainloop()