## Requirements
- python 3
- tkinter (or sudo apt-get install python-tk)
- pip install Pillow 

## Run
- python main_v1.2.py

## Usage
- Click 'load' button to load all images from 'Images' folder (path can be changed in 'Img Dir' area)
- Select on a class on right panel (panel is selected by default)
- Draw a two point rectangle over each object corresponding to the selected class (automaticaly saved)
- Go to next image (click or press arrow buttons)
Annotations files are stored in 'Labels' folder

## Shortcuts
- "Space" cancel current bbox
- "Left" previous image
- "Right" next image
- "w" switch between rectangle/polygon drawing modes