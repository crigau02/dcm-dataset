DCM dataset
=====================

This dataset is composed of 772 annotated images from 27 golden age comic books. We freely collected them from the free public domain collection of digitized comic books [Digital Comics Museum](http://digitalcomicmuseum.com). We selected one album per available publisher to get as many different styles as possible. We made ground-truth bounding boxes of all panels, all characters (body + faces), small or big, human-like or animal-like.

The two image lists from the original paper for the training set and the testing set are available in `original_paper` folder.

Citation ([view full-text](http://www.mdpi.com/2313-433X/4/7/89/htm))
--------------------

	@Article{jimaging4070089,
	AUTHOR = {Nguyen, Nhu-Van and Rigaud, Christophe and Burie, Jean-Christophe},
	TITLE = {Digital Comics Image Indexing Based on Deep Learning},
	JOURNAL = {Journal of Imaging},
	VOLUME = {4},
	YEAR = {2018},
	NUMBER = {7},
	ARTICLE NUMBER = {89},
	URL = {http://www.mdpi.com/2313-433X/4/7/89},
	ISSN = {2313-433X},
	DOI = {10.3390/jimaging4070089}
	}


Ground-truth creation guidelines
----------------

During the annotation process, we have identified four different types of characters that we classified into: human-like, object-like, animal-like and extra (supporting role characters). Human-like are characters that look like humans, such as Robinhood or Batman. Object-like characters are the ones that are similar to objects such as Sponge Bob, Cars, etc. Animal-like could be Garfield, the Pink panther, etc. The extras are characters from any of the classes mentioned earlier but not easily distinguishable or in the shadow. Note that faces have been annotated (when visible) only for human-like class. An example of each class is given in the following image:

![Alt text][img5] Examples of each annotated character class, from left-to-right: human-like, object-like, animal-like and extra.

Panels, body and faces have been annotated with horizontal bounding boxes. Faces are defined as eyebrows, eyes, nose, mouth and chin (with ears if visible), similar to other common datasets from the domain. See examples in the following image:

![Alt text][img6] Examples of annotated bounding boxes for panel (blue), character (red) and face (yellow).

The community can easily extend this dataset because it is based on public domain American comic book images (please use Git request). The Python-based annotation tool can be found in the `tool` folder.

[img5]: http://www.mdpi.com/jimaging/jimaging-04-00089/article_deploy/html/images/jimaging-04-00089-g005.png  "Examples of each annotated character class"
[img6]: http://www.mdpi.com/jimaging/jimaging-04-00089/article_deploy/html/images/jimaging-04-00089-g006.png  "Examples of annotated bounding boxes for panel (blue), character (red) and face (yellow)."

### Annotation format
This repository contains an export of character position and their corresponding class in `groundtruth` folder following this encoding:

`class_id x1 y1 x2 y2`

Where `class_id` corresponds to {"human-like": 1, "object-like": 4, "animal-like":5, "extra":6, "face": 7, "panel": 8}

Images and other annotations such as link between faces, bubbles and character are encoded in SVG and CSV format that can be provided on request to corresponding authors (please send us a request for SFTP access rights).
